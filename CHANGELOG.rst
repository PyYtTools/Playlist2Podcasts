.. this will be appended to README.rst

Changelog
=========

..
   All enhancements and patches to Paylist2Podcasts will be documented
   in this file.  It adheres to the structure of http://keepachangelog.com/ ,
   but in reStructuredText instead of Markdown (for ease of incorporation into
   Sphinx documentation and the PyPI description).

   This project adheres to Semantic Versioning (http://semver.org/).

Unreleased
----------

See the fragment files in the `changelog.d directory`_.

.. _changelog.d directory: https://codeberg.org/PyYtTools/Playlist2Podcasts/src/branch/main/changelog.d

.. scriv-insert-here

.. _changelog-2.0.1:

2.0.1 — 2024-12-22
==================

Fixed
--------

- Container image had incorrect entry point script included.

.. _changelog-2.0.0:

2.0.0 — 2024-12-22
==================

Breaking
--------

- This program now uses a configuration file in `TOML`_ format (instead of json). This should make it easier to create
  and maintain the configuration file.

  This means that you will have to convert or re-create your config file in the `TOML`_ format.

  I have included a utility to read your old (JSON) config file and create a new `TOML`_ config file. To use this tool
  run the following command with `--json_config` pointing to your existing json config file and `--toml_config` pointing
  to the new config file in `TOML`_ format to be created::

    p2p_convert_config --json_config config.json --toml_config config.converted.toml

.. _TOML: https://toml.io/en/

Changed
-------

- Update CI setup to include more checks.

- Updated dependencies versions

.. _changelog-1.1.1:

1.1.1 — 2024-09-16
==================

Changed
-------

- Fixed README.rst and CI

.. _changelog-1.1.0:

1.1.0 — 2024-09-16
==================

Changed
-------

- Changed container creation definition to streamline it

- Updated CI definitions. This includes now automatically building container image.

- Now using `loguru`_ which is optionally configured using a logging-config.toml file.

- Updated dependencies versions

.. _loguru: https://loguru.readthedocs.io/

Removed
-------

- Support for Python 3.8 due to impending drop of official support from python.org for 3.8 during 2024-10

.. _changelog-1.0.0:

1.0.0 — 2024-05-12
==================

Changed
-------

- Updated dependencies

- Now using builtin datetime methods instead of arrow

Removed
-------

- Checking if app is outdated or not. Use standard Python tooling to do this instead (e.g. pip or pipx)

.. _changelog-0.6.8:

0.6.8 — 2023-12-10
==================

Changed
-------

- Change to using `ruff format` as code auto formatter
- Updated dependencies versions

.. _changelog-0.6.7:

0.6.7 — 2023-10-25
==================

Added
-----

- Added CI task to check for vulnerabilities in dependencies on a weekly basis

Changed
-------

- Using typer to configure CLI options

- Refactored some code for better readability and maintainability.

- Container (Docker) image now based on Python 3.12

- Updated dependencies versions

Removed
-------

- Removed dependency groups doc and dev. These are handled in nox and pre-commit

.. _changelog-0.6.6:

0.6.6 — 2023-06-24
==================

Changed
-------

- Updated dependencies.

.. _changelog-0.6.5:

0.6.5 — 2023-03-05
==================

Changed
-------

- Updated dependencies; in particular yt-dlp. See `yt-dlp 2023.03.04`_ for more information on changes introduced with this version of yt-dlp

.. _yt-dlp 2023.03.04: https://github.com/yt-dlp/yt-dlp/releases/tag/2023.03.04

.. _changelog-0.6.4:

0.6.4 — 2023-03-04
==================

Changed
-------

- Updated dependencies; in particular yt-dlp. See `yt-dlp 2023.03.03`_ for more information on changes introduced with this version of yt-dlp

.. _yt-dlp 2023.03.03: https://github.com/yt-dlp/yt-dlp/releases/tag/2023.03.03

.. _changelog-0.6.3:

0.6.3 — 2023-02-20
==================

Changed
-------

- Updated dependencies; in particular yt-dlp. See `yt-dlp 2023.02.17`_ for more information on changes introduced with this version of yt-dlp

.. _yt-dlp 2023.02.17: https://github.com/yt-dlp/yt-dlp/releases/tag/2023.02.17

- Dependency control now using `pdm`_ and releases build and published to Pypi with `flit`_.

.. _pdm: https://pdm.fming.dev/latest/
.. _flit: https://flit.pypa.io/en/latest/

Removed
-------

- Removed poetry references and rstcheck, pip-audit and safety from pre-commit checking. Documentation, pip-audit and safety will still be checked as part of CI workflow.

.. _changelog-0.6.2:

0.6.2 — 2023-01-07
==================

Changed
-------

- Updated dependencies; in particular yt-dlp. See `yt-dlp 2023.01.06`_ for more information on changes introduced with this version of yt-dlp

.. _yt-dlp 2023.01.06: https://github.com/yt-dlp/yt-dlp/releases/tag/2023.01.06

.. _changelog-0.6.1:

0.6.1 — 2023-01-04
==================

Changed
-------

- Changed script inside docker container to run playlist2podcast on an interval

.. _changelog-0.6.0:

0.6.0 — 2023-01-04
==================

Added
-----

- Adding Dockerfile to be able to run as a docker container.

Changed
-------

- Updated dependencies, including `yt-dlp`_. See `yt-dlp 2023.01.02`_ for changes introduced in the yt-dlp update.

.. _yt-dlp: https://github.com/yt-dlp/yt-dlp
.. _yt-dlp 2023.01.02: https://github.com/yt-dlp/yt-dlp/releases/tag/2023.01.02

- Reformatted Changelog to re-structured text format.

- Reformatted Readme to be in reStructured text and add link toe the changelog

.. _changelog-0.5.10:

0.5.10 - 2022.11.22
===================

Changed
-------

- Updated dependencies, including yt-dlp. See `yt-dlp 2022.11.11`_ for changes introduced in the yt-dlp update.

.. _yt-dlp 2022.11.11: https://github.com/yt-dlp/yt-dlp/releases/tag/2022.11.11

.. _changelog-0.5.9:

0.5.9 - 2022-10-07
==================

Changed
-------

- Updated dependencies, including yt-dlp. See `yt-dlp 2022.10.04`_ for changes introduced in the yt-dlp update.

.. _yt-dlp 2022.10.04: https://github.com/yt-dlp/yt-dlp/releases/tag/2022.10.04

.. _changelog-0.5.8:

0.5.8 - 2022-09-13
==================

Changed
-------

- Updated dependencies, including yt-dlp. See `yt-dlp 2022.09.01`_ for changes introduced in the yt-dlp update.

.. _yt-dlp 2022.09.01: https://github.com/yt-dlp/yt-dlp/releases/tag/2022.09.01

.. _changelog-0.5.7:

0.5.7 - 2022-08-20
==================

Changed
-------

- Updated dependencies, including yt-dlp. See `yt-dlp 2022.08.19`_ for changes introduced in the yt-dlp update.

.. _yt-dlp 2022.08.19: https://github.com/yt-dlp/yt-dlp/releases/tag/2022.08.19

.. _changelog-0.5.6:

0.5.6 - 2022-08-15
==================

Added
-------

- `pip-audit`_ to CI pipeline.

.. _pip-audit: https://pypi.org/project/pip-audit/

Changed
-------

- Updated dependencies, including yt-dlp. See `yt-dlp 2022.08.14`_ for changes introduced in the yt-dlp update.
- Replaced httpx with requests as I have found httpx to be unreliable in some edge cases.
- Using rich.print instead of logging for general messages.
  Logging remains for debug messages and warning and more sever messages.

.. _yt-dlp 2022.08.14: https://github.com/yt-dlp/yt-dlp/releases/tag/2022.08.14

.. _changelog-0.5.5:

0.5.5 - 2022-08-10
==================

Added
-------

- Publishing new versions to PyPi.org automatically using CI.

Changed
-------

- Updated dependencies, including yt-dlp. See `yt-dlp 2022.08.08`_ for changes introduced in the yt-dlp update.

.. _yt-dlp 2022.08.08: https://github.com/yt-dlp/yt-dlp/releases/tag/2022.08.08

.. _changelog-0.5.4:

0.5.4 - 2022-07-20
==================

Added
-------

- Added version checking. Now versions are checked against the latest version published on
  `PyPI`_ using the `outdated`_ library

.. _PyPI: https://pypi.org
.. _outdated: https://github.com/alexmojaki/outdated


Changed
-------

- Updated dependencies, including yt-dlp. See `yt-dlp 2022.07.18`_ for changes introduced in the yt-dlp update.

.. _yt-dlp 2022.07.18: https://github.com/yt-dlp/yt-dlp/releases/tag/2022.07.18

.. _changelog-0.5.3:

0.5.3 - 2022-06-29
==================

Changed
-------

- Updated dependencies, including yt-dlp. See `yt-dlp 2022.06.29`_ for changes introduced in the yt-dlp update.

.. _yt-dlp 2022.06.29: https://github.com/yt-dlp/yt-dlp/releases/tag/2022.06.29

.. _changelog-0.5.2:

0.5.2 - 2022-06-23
==================

Changed
-------

- Updated dependencies.

.. _changelog-0.5.1:

0.5.1 - 2022-06-05
==================

Changed
-------

- Updated dependencies.

.. _changelog-0.5.0:

0.5.0 - 2022-05-14
==================

Added
-------

- Include and exclude filters are now also applied to the 'original_url' of a palylist entry.
  This allows easy exclusion or inclusion of youtube shorts by adding `.*shorts.*` as a filter.

Changed
-------

- Limited the number of videos to consider to 3 time the number of episodes to add to podcast feed.


Removed
-------

- Checks for updates. This seems not to be necessary anymore. With this app being on Pypi.org version checks can easily
  be done using Pip or Pipx.

.. _changelog-0.4.3:

0.4.3 - 2022-04-10
==================

Changed
-------

- Updated dependencies.

.. _changelog-0.4.2:

0.4.2 - 2022-03-10
==================

Changed
-------

- Updated dependencies.

.. _changelog-0.4.1:

0.4.1 - 2022-02-06
==================

Changed
-------

- Updated dependencies.

.. _changelog-0.4.0:

0.4.0 - 2022-01-30
==================

Changed
-------

- Changed from using youtube_dl to now using yt-dlp
- Updated dependencies / requirements

.. _changelog-0.3.1:

0.3.1 - 2022-01-23
==================

Fixed
-------

- repackaging in module playlist2podcast to make releasing to PyPi work as another project is already using
  the package name ogma. Dropping all mentions of ogma

.. _changelog-0.3.0:

0.3.0 - 2022-01-23
==================

Added
-------

- Include/exclude filters per playlist
- Automatic migration of config to new version
- First set of unit tests. Also run during CI

Fixed
-------

- Changed last occurrences of old name to current name, `Playlist2Podcast`

Changed
-------

- Change format of config file and included automatic migration of prior format to new format.
- Moved code into package named `ogma` (after the Celtic god `Ogma - The God of Speech & Language`_)


.. _Ogma - The God of Speech & Language: https://druidry.org/resources/ogma-the-god-of-speech-language

Removed
-------

- `vulture` from CI

.. _changelog-0.2.4:

0.2.4 - 2022-01-14
==================

- Added dlint and eradicate to flake8 tests
- Removed potentially insecure code pointed out by dlint
- Added radon metrics to Woodpecker CI
- Updated dependencies / requirements versions

.. _changelog-0.2.3:

0.2.3 - 2022-01-09
==================

- Allow using a cookie file to help with downloading from YouTube.
- Implemented Poetry to control dependencies / requirements (instead of requirements.txt
- Using Woodpecker CI for rudimentary quality control.

.. _changelog-0.2.2:

0.2.2 - 2021-03-21
==================

- Bug fix to stop erroneous removal of episode files
- Renamed python program file to playlist2podcast.py

.. _changelog-0.2.1:

0.2.1 - 2021-03-19
==================

- Bug fix to allow episodes / videos from all playlists to included.

.. _changelog-0.2.0:

0.2.0 - 2021-03-19
==================

- Add possibility to check multiple playlists and download as needed

.. _changelog-0.1.0:

0.1.0 - 2021-03-18
==================

- Initial release able to download videos from one playlist
