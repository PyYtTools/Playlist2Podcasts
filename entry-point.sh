#!/bin/sh
set -e

cd /run

if test -z "${LOGGING_CONFIG}" ; then
	LOGGING="--logging-config=/logging/logging-config.toml"
else
	LOGGING="--logging-config=${LOGGING_CONFIG}"
fi

if test -z "${UPDATE_INTERVAL}" ; then
	echo "UPDATE_INTERVAL not set, exiting!"
	exit 1
fi

while true
do
    uv run playlist2podcast --config-file /config/config.toml --publish-dir /publish "${LOGGING}"

    echo ""
    echo "Waiting for $UPDATE_INTERVAL until next update. It is now $(date -Iseconds)"
    sleep "${UPDATE_INTERVAL}"
done
