# Container creation definition

FROM ghcr.io/astral-sh/uv:debian-slim AS builder
ARG TARGETARCH

# hadolint ignore=DL3008
RUN apt-get update \
    && apt-get install -y --no-install-recommends wget xz-utils ca-certificates

WORKDIR /ffmpeg
RUN wget --progress=dot:giga https://johnvansickle.com/ffmpeg/builds/ffmpeg-git-"${TARGETARCH}"-static.tar.xz \
    && wget --progress=dot:giga https://johnvansickle.com/ffmpeg/builds/ffmpeg-git-"${TARGETARCH}"-static.tar.xz.md5 \
    && md5sum -c ffmpeg-git-"${TARGETARCH}"-static.tar.xz.md5 \
    && tar -xf ffmpeg-git-"${TARGETARCH}"-static.tar.xz \
    && cat ffmpeg-git*/readme.txt

FROM ghcr.io/astral-sh/uv:debian-slim

LABEL maintainer="marvin8 <marvin8@tuta.io>"

COPY --from=builder /ffmpeg/ffmpeg-git-*/ffprobe /usr/local/bin
COPY --from=builder /ffmpeg/ffmpeg-git-*/ffmpeg /usr/local/bin
COPY entry-point.sh /run/

# Install app and dependencies
WORKDIR /run
RUN --mount=source=dist,target=/dist \
    ffmpeg -version \
    && uv --version \
    && uv venv -p 3.12.8 \
    && uv pip install /dist/*.whl

VOLUME /config
VOLUME /publish
VOLUME /logging

ENTRYPOINT ["/run/entry-point.sh"]
