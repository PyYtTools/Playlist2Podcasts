"""Unit tests for filtering function 'is_video_included'."""

from playlist2podcast.playlist_2_podcast import is_video_included


def test_no_filters() -> None:
    """Test filtering without any filters."""
    response = is_video_included(
        video_title="No Filters",
        video_original_url="No Filters",
        include_filters=[],
        exclude_filters=[],
    )
    assert response is True  # noqa: S101


def test_matching_include_filter_only() -> None:
    """Test filtering with include filters only."""
    response = is_video_included(
        video_title="No Filters",
        video_original_url="No Filters",
        include_filters=["^No.*"],
        exclude_filters=[],
    )
    assert response is True  # noqa: S101


def test_matching_one_of_multiple_include_filters() -> None:
    """Test multiple include filters."""
    response = is_video_included(
        video_title="No Filters",
        video_original_url="No Filters",
        include_filters=["^No.*", "not matching"],
        exclude_filters=[],
    )
    assert response is True  # noqa: S101


def test_matching_all_of_multiple_include_filters() -> None:
    """Test include all include filters matching."""
    response = is_video_included(
        video_title="No Filters",
        video_original_url="No Filters",
        include_filters=["^No.*", ".*Filter.*"],
        exclude_filters=[],
    )
    assert response is True  # noqa: S101


def test_matching_exclude_filter_only() -> None:
    """Test exclude filter only."""
    response = is_video_included(
        video_title="No Filters",
        video_original_url="No Filters",
        include_filters=[],
        exclude_filters=[".*No.*"],
    )
    assert response is False  # noqa: S101


def test_matching_one_of_multiple_exclude_filters() -> None:
    """Test one of many exclude filters matching."""
    response = is_video_included(
        video_title="No Filters",
        video_original_url="No Filters",
        include_filters=[],
        exclude_filters=[".*No.*", "not matching"],
    )
    assert response is False  # noqa: S101


def test_matching_all_of_multiple_exclude_filters() -> None:
    """Test all exclude filters matching."""
    response = is_video_included(
        video_title="No Filters",
        video_original_url="No Filters",
        include_filters=[],
        exclude_filters=[".*No.*", ".*Filter.*"],
    )
    assert response is False  # noqa: S101


def test_matching_include_and_exclude_filters() -> None:
    """Test include and exclude filter matching."""
    response = is_video_included(
        video_title="No Filters",
        video_original_url="No Filters",
        include_filters=["^No.*"],
        exclude_filters=[".*Filter.*"],
    )
    assert response is False  # noqa: S101


def test_specific_level1_news_episode() -> None:
    """Test specific Level1 News filter."""
    response = is_video_included(
        video_title="Level1 News January 21 2022: Driving Miss Deer",
        video_original_url="No Filters",
        include_filters=["^Level1 News.*"],
        exclude_filters=[],
    )
    assert response is True  # noqa: S101
