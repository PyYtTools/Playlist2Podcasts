# ruff: noqa: D100, D102, D103, S101, S106
"""Unit tests for filtering function 'is_video_included'."""

from datetime import datetime
from datetime import timezone
from pathlib import Path

from feedgen.feed import FeedEntry

from playlist2podcast.playlist_2_podcast import Config
from playlist2podcast.playlist_2_podcast import create_feed_entry


def test_create_feed_entry() -> None:
    """Test create_feed_entry method."""
    video = {
        "upload_date": "20241222",
        "uploader": "Mr Testy McTest",
        "id": "test_id",
        "webpage_url": "https://example.url",
        "title": "Podcast Title",
        "description": "long explanation",
        "duration": "73",
    }
    config = Config(
        version="version",
        podcast_host="http://podcast.host",
        media_dir="media_dir",
    )
    feed_entry: FeedEntry = create_feed_entry(
        video=video,
        thumbnail=Path("thumbnail.png"),
        host_audio_file="audio_file_path",
        config=config,
    )

    assert feed_entry is not None
    assert feed_entry.author()[0].get("name") == "Mr Testy McTest"
    assert feed_entry.id() == "test_id"
    assert feed_entry.link()[0].get("href") == "https://example.url"
    assert feed_entry.title() == "Podcast Title"
    assert feed_entry.description() == "long explanation"
    assert feed_entry.published() == datetime(2024, 12, 22, 0, 0, 0, 0, timezone.utc)
    assert feed_entry.podcast.itunes_image() == "http://podcast.host/media_dir/thumbnail.png"
    assert feed_entry.podcast.itunes_duration() == "73"
    assert feed_entry.enclosure().get("url") == "audio_file_path"
